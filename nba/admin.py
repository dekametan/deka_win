from django.contrib import admin

# Register your models here.
from nba.models import Team, Schedule, Game, Elo

admin.site.register([Team, Schedule, Game, Elo])
