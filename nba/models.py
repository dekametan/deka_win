from django.db import models


# Create your models here.
class Team(models.Model):

    full_name = models.CharField(max_length=100)
    alias = models.CharField(max_length=3)
    conference = models.CharField(max_length=1)
    elo = models.FloatField(max_length=6)

    def __str__(self):
        return self.full_name + ' (' + self.alias + ')'


class Schedule(models.Model):
    date = models.CharField(max_length=10)
    home_team = models.CharField(max_length=3)
    away_team = models.CharField(max_length=3)

    def __str__(self):
        return self.date + ' ' + self.away_team + ' vs ' + self.home_team


class Game(models.Model):
    game_number = models.PositiveSmallIntegerField()
    date = models.CharField(max_length=10)
    time = models.CharField(max_length=5)
    home_team = models.CharField(max_length=3)
    is_home = models.PositiveSmallIntegerField()
    away_team = models.CharField(max_length=3)
    result = models.PositiveSmallIntegerField()
    is_ot = models.PositiveSmallIntegerField()
    home_pts = models.PositiveSmallIntegerField()
    away_pts = models.PositiveSmallIntegerField()
    last_game_result = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.away_team + ' vs ' + self.home_team + ' on date ' + self.date


class Elo(models.Model):
    date = models.CharField(max_length=10)
    team = models.CharField(max_length=3)
    elo = models.FloatField(max_length=7)

    def __str__(self):
        return self.date + ' ' + self.team + ' - ' + str(self.elo)