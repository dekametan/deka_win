import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf
import sqlite3
import os

# date of games
game_date = '2018-11-15'


conn = sqlite3.connect('/Users/user/Documents/deka_win/db.sqlite3')
c = conn.cursor()
cc = conn.cursor()
ccc = conn.cursor()

c.execute("SELECT DISTINCT home_team, away_team FROM nba_schedule WHERE date = ?", (game_date,))
teams = c.fetchall()

print(game_date)
print()
print('Number of games: ' + str(len(teams)))
print('Number of bets: ' + str(len(teams) * 2))
print('')
print('----------------')


for team in teams:
    for j in range(0, 2):

        df_adv = pd.read_csv('season_results/result_' + team[j] + '.csv')
        X = df_adv[['is_home', 'elo_opp', 'elo_home', 'last_game_result', 'away_team_style']]
        y = df_adv['points']

        X = sm.add_constant(X)
        est = sm.OLS(y, X).fit()

        y_pred = est.predict(X)

        lmod = smf.ols(formula=
                       'points ~ is_home + elo_opp + elo_home + last_game_result + away_team_style', data=df_adv).fit()

        x0 = df_adv.loc[:, ("is_home", "elo_opp", "elo_home", "last_game_result", "away_team_style")].median()
        x0 = pd.np.append(1, x0.ravel())

        print(team[j], round(float(pd.np.dot(x0, lmod.params))))
