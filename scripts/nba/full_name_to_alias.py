# replaces full name to alias
def replace_full_name(team):

    if team in 'Atlanta Hawks':
        team = 'ATL'
    if team in 'Boston Celtics':
        team = 'BOS'
    if team in 'Brooklyn Nets' or team in 'New Jersey Nets':
        team = 'BRK'
    if team in 'Charlotte Hornets' or team in 'Charlotte Bobcats':
        team = 'CHO'
    if team in 'Chicago Bulls':
        team = 'CHI'
    if team in 'Cleveland Cavaliers':
        team = 'CLE'
    if team in 'Dallas Mavericks':
        team = 'DAL'
    if team in 'Denver Nuggets':
        team = 'DEN'
    if team in 'Detroit Pistons':
        team = 'DET'
    if team in 'Golden State Warriors':
        team = 'GSW'
    if team in 'Houston Rockets':
        team = 'HOU'
    if team in 'Indiana Pacers':
        team = 'IND'
    if team in 'Los Angeles Clippers':
        team = 'LAC'
    if team in 'Los Angeles Lakers':
        team = 'LAL'
    if team in 'Memphis Grizzlies':
        team = 'MEM'
    if team in 'Miami Heat':
        team = 'MIA'
    if team in 'Milwaukee Bucks':
        team = 'MIL'
    if team in 'Minnesota Timberwolves':
        team = 'MIN'
    if team in 'New Orleans Pelicans' or team in 'New Orleans Hornets':
        team = 'NOP'
    if team in 'New York Knicks':
        team = 'NYK'
    if team in 'Oklahoma City Thunder' or team in 'Seattle Supersonics':
        team = 'OKC'
    if team in 'Orlando Magic':
        team = 'ORL'
    if team in 'Philadelphia 76ers':
        team = 'PHI'
    if team in 'Phoenix Suns':
        team = 'PHO'
    if team in 'Portland Trail Blazers':
        team = 'POR'
    if team in 'Sacramento Kings':
        team = 'SAC'
    if team in 'San Antonio Spurs':
        team = 'SAS'
    if team in 'Toronto Raptors':
        team = 'TOR'
    if team in 'Utah Jazz':
        team = 'UTA'
    if team in 'Washington Wizards':
        team = 'WAS'

    return team
