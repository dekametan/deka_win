import csv
import sqlite3

import os
from bs4 import BeautifulSoup
import requests
from dateutil.parser import parse
from fake_useragent import UserAgent

from scripts.full_name_to_alias import replace_full_name

ua = UserAgent()

year = '2018'
months = ['october', 'november', 'december', 'january', 'february', 'march', 'april']

conn = sqlite3.connect(str(os.path.abspath(os.pardir).split('/scripts')[0]) + '/db.sqlite3')
c = conn.cursor()

with open('season_results/season_schedule.csv', 'w', newline='') as csv_file:
    fieldnames = ['date', 'home_team', 'away_team']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()
    for month in months:

        # example of url - https://www.basketball-reference.com/players/c/conlemi01/gamelog/2017/
        url = 'https://www.basketball-reference.com/leagues/NBA_' + year + '_games-' + month + '.html'
        print(url)

        # random user-agent
        r = requests.get(url, ua.random)

        # print current player and his number in list
        print('schedule for ' + month)

        if r.status_code == 200:

            # Get text
            rr = r.text

            # Parse HTML
            soup = BeautifulSoup(rr, 'html.parser')

            data = []
            if soup.find_all('table', attrs={'id': 'schedule'}):
                table = soup.find_all('table', attrs={'id': 'schedule'})

                table_team = table[0].find('tbody')
                rows_team = table_team.find_all('tr')

                # Function finds all td in table (tbody of table is argument)
                def write_stat(rows):

                    for row in rows:
                        cols = row.find_all('td')
                        cols = [ele.text.strip() for ele in cols]

                        cols_th = row.find_all('th')
                        cols_th = [ele.text.strip() for ele in cols_th]

                        c = cols_th + cols

                        data.append(c)

                write_stat(rows_team)

                for i in range(0, len(data)):

                    dt = parse(data[i][0])

                    data[i][0] = dt.date().strftime('%Y-%m-%d')

                    date = data[i][0]

                    data[i][2] = replace_full_name(data[i][2])
                    data[i][4] = replace_full_name(data[i][4])

                    # print(data[i])

                    writer.writerow(
                        {'date': date, 'home_team': data[i][2], 'away_team': data[i][4]})

                    c.execute("INSERT INTO nba_schedule(date, home_team, away_team) VALUES (?, ?, ?)", (date, data[i][2], data[i][4]))
                    conn.commit()


conn.close()
