import sqlite3
import csv


# parameter date is from date start collecting data (example: '2017-10-01')


def generate_csv_team(date):

    conn = sqlite3.connect('/Users/user/Documents/deka_win/db.sqlite3')
    cc = conn.cursor()
    ccc = conn.cursor()
    cccc = conn.cursor()
    cccc.execute("SELECT alias FROM nba_team")
    teams = cccc.fetchall()

    for team in teams:

        file = open('season_results/result_' + team[0] + '.csv', 'w+')
        cc.execute("SELECT is_home, result, is_ot, home_pts, away_pts, elo, last_game_result FROM nba_game JOIN nba_elo WHERE nba_game.date = nba_elo.date_elo AND nba_game.date >= ? AND home_team = team AND home_team = ?", (date, team[0]))
        ccc.execute("SELECT elo FROM nba_elo JOIN nba_game WHERE nba_elo.date = nba_elo.date AND nba_elo.date >= ? AND opp_team = team", (date,))
        b = cc.fetchall()
        c = ccc.fetchall()

        file.write('"is_home","result","is_ot","points","opp_points","elo_home","elo_opp"' + '\n')
        for i in range(0, len(b)):

            first = list(b[i])
            second = list(c[i])

            if first[0] == 0:
                first[0] = 1
            elif first[0] == 1:
                first[0] = 0

            result = first + second

            wr = csv.writer(file, quoting=csv.QUOTE_NONE)
            wr.writerow(result)

        print('---CSV for ' + team[0] + ' is generated---')
        file.close()
