import csv
import sqlite3

import os
import requests
from dateutil.parser import parse as date_parse

# nba team's alias
teams = [['hawks', 'ATL'],
         ['celtics', 'BOS'],
         ['nets', 'BRK'],
         ['hornets', 'CHO'],
         ['bulls', 'CHI'],
         ['cavaliers', 'CLE'],
         ['mavericks', 'DAL'],
         ['nuggets', 'DEN'],
         ['pistons', 'DET'],
         ['warriors', 'GSW'],
         ['rockets', 'HOU'],
         ['pacers', 'IND'],
         ['clippers', 'LAC'],
         ['lakers', 'LAL'],
         ['grizzlies', 'MEM'],
         ['heat', 'MIA'],
         ['bucks', 'MIL'],
         ['timberwolves', 'MIN'],
         ['pelicans', 'NOP'],
         ['knicks', 'NYK'],
         ['thunder', 'OKC'],
         ['magic', 'ORL'],
         ['sixers', 'PHI'],
         ['suns', 'PHO'],
         ['trailblazers', 'POR'],
         ['kings', 'SAC'],
         ['spurs', 'SAS'],
         ['raptors', 'TOR'],
         ['jazz', 'UTA'],
         ['wizards', 'WAS']]

# start get elo's history from year
year = 2017
season_start_date = '2017-10-01'

conn = sqlite3.connect(str(os.path.abspath(os.pardir).split('/scripts')[0]) + '/db.sqlite3')
c = conn.cursor()

with open('season_results/season_elo.csv', 'w', newline='') as csv_file:
    fieldnames = ['date', 'team', 'elo']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    writer.writeheader()

    for team in teams:

        url = 'https://projects.fivethirtyeight.com/complete-history-of-the-nba/data/' + team[0] + '.json?v=23'

        response = requests.get(url)
        json_result = response.json()

        for i in range(1, len(json_result['value'])):

            if 'd' in json_result['value'][i]:

                # parse date
                dt = date_parse(json_result['value'][i]['d'])

                # check if date >= season start date
                if dt >= date_parse(season_start_date):

                    # format date
                    date = dt.date().strftime('%Y-%m-%d')

                    print('For ' + team[1] + ' vs ' + json_result['value'][i]['t'] + ' Elo is ' + str(json_result['value'][i]['y']) + ' on ' + date)

                    team_elo = team[1]

                    # elo value
                    elo = json_result['value'][i]['y']

                    writer.writerow(
                        {'date': date, 'team': team_elo, 'elo': elo})

                    conn.execute("INSERT INTO nba_elo(date, team, elo) VALUES (?, ?, ?)", (date, team_elo, elo))
                    conn.commit()

conn.close()
