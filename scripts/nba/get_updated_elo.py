from bs4 import BeautifulSoup
import requests
from scripts.full_name_to_alias import replace_full_name


def update_elo(year):

    # get elo
    url = 'https://projects.fivethirtyeight.com/' + str(year) + '-nba-predictions/'
    headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'}
    r = requests.get(url, headers=headers)

    if r.status_code == 200:

        # Get text
        rr = r.text

        # Parse HTML
        soup = BeautifulSoup(rr, 'html.parser')

        data = []
        table = soup.find_all('table', attrs={'id': 'standings-table'})

        table_team = table[0].find('tbody')

        rows_team = table_team.find_all('tr')

        # Function finds all td in table (tbody of table is argument)
        def write_stat(rows):

            for row in rows:

                cols = row.find_all('td')
                cols = [ele.text.strip() for ele in cols]

                cols_th = row.find_all('th')
                cols_th = [ele.text.strip() for ele in cols_th]

                c = cols_th + cols

                data.append(c)

            elo = []
            for i in range(0, 30):

                team = replace_full_name(data[i][3][:-5])
                elo.append([int(data[i][0]), team])

            print(elo)
        write_stat(rows_team)

    print('')
    print('---Elos are updated---')
    print('')


update_elo(2018)
