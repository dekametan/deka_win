import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf
import sqlite3
import os

# date of games
game_date = '2018-11-14'


conn = sqlite3.connect('/Users/user/Documents/deka_win/db.sqlite3')
c = conn.cursor()
cc = conn.cursor()
ccc = conn.cursor()

c.execute("SELECT DISTINCT home_team, away_team FROM nba_schedule WHERE date = ?", (game_date,))
teams = c.fetchall()

print(teams)

print(game_date)
print()
print('Number of games: ' + str(len(teams)))
print('Number of bets: ' + str(len(teams) * 2))
print('')
print('----------------')


for team in teams:
    for j in range(0, 2):

        df_adv = pd.read_csv('season_results/result_' + team[j] + '.csv')
        X = df_adv[['is_home', 'elo_opp', 'elo_home', 'last_game_result', 'away_team_style']]
        y = df_adv['points']

        X = sm.add_constant(X)
        est = sm.OLS(y, X).fit()

        y_pred = est.predict(X)

        # est_2 = sm.ols(formula='points ~ is_home + elo_opp + elo_home', data=df_adv).fit()
        # print(est_2.summary())

        lmod = smf.ols(formula=
                       'points ~ is_home + elo_opp + elo_home + last_game_result + away_team_style', data=df_adv).fit()

        x0 = df_adv.loc[:, ("is_home", "elo_opp", "elo_home", "last_game_result", "away_team_style")].median()
        x0 = pd.np.append(1, x0.ravel())

        print(x0)
        print(pd.np.dot(x0, lmod.params))

        # print('R-squared for ' + team[0] + ' is ' + str(round(est.rsquared, 4)))
        # print('Prediction % ' + str(round((est.rsquared * 100), 1)))
        # print('')

        elo_home_query = cc.execute("SELECT elo, game_style FROM nba_team WHERE alias = ?", (team[0], ))
        elo_opp_query = ccc.execute("SELECT elo, game_style FROM nba_team WHERE alias = ?", (team[1], ))

        elo_home_t = elo_home_query.fetchone()
        elo_opp_t = elo_opp_query.fetchone()

        elo_home = elo_home_t[0]
        elo_opp = elo_opp_t[0]

        game_style_home = elo_home_t[1]
        game_style_away = elo_opp_t[1]

        const = est.params[0]
        is_home_param = est.params[1]
        elo_opp_param = est.params[2]
        elo_home_param = est.params[3]

        last_game_result_param = est.params[4]

        away_team_style_param = est.params[5]

        predict_home_team = 1 * is_home_param + elo_opp * elo_opp_param + elo_home * elo_home_param + game_style_away * away_team_style_param + const
        predict_away_team = 0 * is_home_param + elo_home * elo_opp_param + elo_opp * elo_home_param + const

        predict_total = predict_home_team + predict_away_team

        print(str(team[0]) + "(HOME" + ", " + str(elo_home) + "): " + str(round(predict_home_team, 1)))
        print(str(team[1]) + "(AWAY" + ", " + str(elo_opp) + "): " + str(round(predict_away_team, 1)))
        print("Total: " + str(round(predict_total, 1)))
        print('----------------')
        print('')
    # print(est.summary())

# https://projects.fivethirtyeight.com/2018-nba-predictions/
# DELETE FROM elo_elo WHERE date_elo like '2017%';
# DELETE FROM games_game WHERE date >= '2017-10-01';
