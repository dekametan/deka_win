import sqlite3

import os
import requests
import time
import csv
from bs4 import BeautifulSoup
from dateutil.parser import parse
from scripts.full_name_to_alias import replace_full_name

# nba team's alias
teams = [['hawks', 'ATL'],
         ['celtics', 'BOS'],
         ['nets', 'BRK'],
         ['hornets', 'CHO'],
         ['bulls', 'CHI'],
         ['cavaliers', 'CLE'],
         ['mavericks', 'DAL'],
         ['nuggets', 'DEN'],
         ['pistons', 'DET'],
         ['warriors', 'GSW'],
         ['rockets', 'HOU'],
         ['pacers', 'IND'],
         ['clippers', 'LAC'],
         ['lakers', 'LAL'],
         ['grizzlies', 'MEM'],
         ['heat', 'MIA'],
         ['bucks', 'MIL'],
         ['timberwolves', 'MIN'],
         ['pelicans', 'NOP'],
         ['knicks', 'NYK'],
         ['thunder', 'OKC'],
         ['magic', 'ORL'],
         ['sixers', 'PHI'],
         ['suns', 'PHO'],
         ['trailblazers', 'POR'],
         ['kings', 'SAC'],
         ['spurs', 'SAS'],
         ['raptors', 'TOR'],
         ['jazz', 'UTA'],
         ['wizards', 'WAS']]

start_time = time.time()

count = 0
year = 2017

conn = sqlite3.connect(str(os.path.abspath(os.pardir).split('/scripts')[0]) + '/db.sqlite3')
c = conn.cursor()

# file for all results
file = open('season_results/season_stat_all.csv', 'w', newline='')
# csv header
fieldnames = ['game_number', 'date', 'time', 'home_team', 'is_home', 'away_team',
              'result', 'is_ot', 'home_pts', 'away_pts', 'last_game_result']
writer_all = csv.DictWriter(file, fieldnames=fieldnames)
writer_all.writeheader()

for year in range(year, 2018):

    for alias in teams:

        short_name = alias[1]

        # check if team was renamed
        if year < 2013 and short_name == 'BRK':
            short_name = 'NJN'
        elif year > 2012 and short_name == 'NJN':
            short_name = 'BRK'

        if year < 2009 and short_name == 'OKC':
            short_name = 'SEA'
        elif year > 2008 and short_name == 'SEA':
            short_name = 'OKC'

        if year < 2015 and short_name == 'CHO':
            short_name = 'CHA'
        elif year > 2014 and short_name == 'CHA':
            short_name = 'CHO'

        if year < 2014 and short_name == 'NOP':
            short_name = 'NOH'
        elif year > 2013 and short_name == 'NOH':
            short_name = 'NOP'

        # csv files generating
        with open('season_results/season_stat_' + short_name + '_' + str(year) + '.csv', 'w', newline='') as csv_file:

            # csv header
            fieldnames = ['game_number', 'date', 'time', 'home_team', 'is_home', 'away_team',
                          'result', 'is_ot', 'home_pts', 'away_pts', 'last_game_result']
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writeheader()

            url = 'https://www.basketball-reference.com/teams/' + short_name + '/' + str(year) + '_games.html'
            headers = {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'}
            r = requests.get(url, headers=headers)

            print(str(count) + '. ' + url)
            count += 1

            if r.status_code == 200:

                # Get text
                rr = r.text

                # Parse HTML
                soup = BeautifulSoup(rr, 'html.parser')

                data = []
                table = soup.find_all('table', attrs={'id': 'games'})

                table_team = table[0].find('tbody')

                rows_team = table_team.find_all('tr')

                # Function finds all td in table (tbody of table is argument)
                def write_stat(rows):

                    for row in rows:

                        cols = row.find_all('td')
                        cols = [ele.text.strip() for ele in cols]

                        cols_th = row.find_all('th')
                        cols_th = [ele.text.strip() for ele in cols_th]

                        c = cols_th + cols

                        data.append(c)

                    rows_to_delete = [20, 40, 60, 80]

                    for row_to_delete in rows_to_delete:
                        try:
                            del data[row_to_delete]

                        # 2012 is year of lockout
                        except:
                            print('No such row #' + str(row_to_delete) + ' on year ' + str(year))

                    for i in range(0, len(data)):

                        # date formatting
                        data[i][1] = parse(data[i][1]).date().strftime('%Y-%m-%d')

                        # time formatting
                        data[i][2] = parse(data[i][2]).time().strftime('%H:%M')

                        # if '@' team played at home
                        if data[i][5] == '@':
                            data[i][5] = 1
                        else:
                            data[i][5] = 0

                        # is team won or lose
                        if data[i][7] == 'W':
                            data[i][7] = 1
                        if data[i][7] == 'L':
                            data[i][7] = 0

                        # full_name to alias
                        data[i][6] = replace_full_name(data[i][6])

                        # is OT
                        if 'OT' in data[i][8]:
                            data[i][8] = 1
                        else:
                            data[i][8] = 0

                        # is last game win
                        if 'W' in data[i][-2] and '1' not in data[i][-2] or 'L' in data[i][-2] and '1' in data[i][-2]:
                            data[i][-2] = 1
                        elif 'L' in data[i][-2] and '1' not in data[i][-2] or 'W' in data[i][-2] and '1' in data[i][-2]:
                            data[i][-2] = 0

                        # insert team alias
                        data[i].insert(5, short_name)

                        # stat for each team
                        writer.writerow({'game_number': data[i][0], 'date': data[i][1], 'time': data[i][2], 'home_team': data[i][5],
                                         'is_home': data[i][6], 'away_team': data[i][7], 'result': data[i][8], 'is_ot': data[i][9],
                                         'home_pts': data[i][10], 'away_pts': data[i][11], 'last_game_result': data[i][14]})

                        # write data of all games stat
                        writer_all.writerow(
                            {'game_number': data[i][0], 'date': data[i][1], 'time': data[i][2], 'home_team': data[i][5],
                             'is_home': data[i][6], 'away_team': data[i][7], 'result': data[i][8], 'is_ot': data[i][9],
                             'home_pts': data[i][10], 'away_pts': data[i][11], 'last_game_result': data[i][14]})

                        conn.execute("INSERT INTO nba_game(game_number, date, time, home_team, is_home, away_team, "
                                     "result, is_ot, home_pts, away_pts, last_game_result) VALUES (?, ?, ?, ?, ?, ?, "
                                     "?, ?, ?, ?, ?)", (data[i][0], data[i][1], data[i][2], data[i][5], data[i][6],
                                                     data[i][7], data[i][8], data[i][9], data[i][10], data[i][11], data[i][14]))
                        conn.commit()

                    print(data)
                write_stat(rows_team)

end_time = time.time()

result_time = round(end_time - start_time)

print()
print('Script\'s time execution: ' + str(result_time) + 's (' + str(round(result_time/60)) + 'min)')

conn.close()
